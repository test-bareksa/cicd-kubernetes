package main

import (
    "testing"
    "net/http"
    "net/http/httptest"
)

func TestGetPeople(t *testing.T) {
    req, err := http.NewRequest("GET", "/", nil)
    if err != nil {
        t.Fatal(err)
    }

    rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GetPeople)
	// handler := router()
	handler.ServeHTTP(rr, req)
	// fmt.Print(rr.Body)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	//Check the response body is what we expect.
	expected := `[{"id":"1","firstname":"John","lastname":"Doe","address":{"city":"City X","state":"State X"}},{"id":"2","firstname":"Koko","lastname":"Crunch","address":{"city":"City Y","state":"State V"}},{"id":"3","firstname":"Kuli","lastname":"Ner","address":{"city":"City Z","state":"State X"}},{"id":"4","firstname":"Jaka","lastname":"Tarub","address":{"city":"City G","state":"State Y"}}]`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
